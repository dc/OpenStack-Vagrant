#!/usr/bin/env ruby

################################################################################
# Input: Number of compute nodes
# Output: Playbook, host.j2, variables file
################################################################################
require 'json'
require 'optparse'
require 'pp'

################################################################################
# Class & Function definitions
################################################################################
class InputParser
    @options = {
      'computeNodes' => {'flag' => 'n', 'desc' => 'Number of compute nodes', 'cast' => OptionParser::DecimalInteger, 'default' => 1},
      'computePath' => {'flag' => 'f', 'desc' => '.json file with compute nodes {ip1:{hostname1:,role1:},...}}', 'default' => ""},
      'playbookPath' => {'flag' => 'p', 'desc' => 'playbook.yml path', 'default' => 'playbook.yml'},
      'hostsPath' => {'flag' => 'i', 'desc' => 'hosts.j2 path', 'default' => 'roles/common/templates/hosts.j2'},
      'yes' => {'flag' => 'y', 'desc' => 'Do not ask for user confirmation', 'type' => 'switch', 'default' => false}
    }

  def self.parse(args)

    @options.each do |_k, v|
      v['cast'] = String if v['cast'].nil?
      v['type'] = 'option' if v['type'].nil?
    end # set normal casting to String and type to "option"

    result = {}

    OptionParser.new do |opts|

      opts.banner = "Usage: SetupVagrant.rb [options]"
      opts.on('-h', '--help', 'Display this screen') do
        puts opts
        exit
      end

      @options.each do |k, v|
        result[k] = v['default']
        if v['type'] == 'option'
          opts.on("-#{v['flag']} [operand]", "--#{k} [operand]", v['cast'], v['desc']) { |operand| result[k] = operand }
        else
          opts.on("-#{v['flag']}", "--#{k}", v['cast'], v['desc']) { result[k] = !v['default'] }
        end

      end

    end.parse!(args)
    # ask for user input

    pp result
    if result['yes'] == false
      pp 'Run with these Options?[y/n]'
      answer = gets.chomp
      exit unless answer == 'y' || answer == 'Y'
    end
    result # return options
  end # self.parse()
end

def create_nodes(compNum, path='n.json', ip_base='10.0.0.')
  """
  Create 1 controller and N compute, save the dictionary in a .json
  and return it.
  """

  # List of IP directions for controller and compute nodes
  ips = (0..compNum).map{|n| ip_base + (10 * (2 * n + 1) + 1).to_s}
  # List of hostnames for controller and compute nodes
  hostnames = ['controller'] + (1..compNum).map{|n| 'compute'+n.to_s}
  # List of roles for controller and comptue nodes
  roles = ['controller'] + (1..compNum).map{|n| 'compute'}

  # Individual dictionaries for each node
  node = (0..compNum).map{|n| Hash[ ['hostname', 'role'].zip([hostnames[n], roles[n]]) ]}

  # Create THE dictionary using ips as keys
  nodes = Hash[ips.zip(node)]

  # Create and write dictionary in a .json
  File.open(path, "w") do |f|
    f.write(JSON.pretty_generate(nodes))
  end

  return ips, hostnames, roles
end

def read_nodes(nodeFile)
  """
  Reads nodes from a .json file
  Structure (all strings): { hostname1:{interface1:{ip1:,netmask1:}, role1:}, ...}
  """
  nodes = File.read(nodeFile)
  return JSON.parse(nodes)
end


################################################################################
# Parsing options
################################################################################

options=InputParser.parse(ARGV)
################################################################################
# Actual program
################################################################################

# Read the .json file if given. Otherwise, create the specified number of nodes.
nodes = (options["computePath"]!="" ? read_nodes(options["computePath"]) : create_nodes(options["computeNodes"]))
controlname = nodes.select{|host,v|v['role']=='controller'}.keys[0]
computenames = nodes.select{|host,v|v['role']=='compute'}.keys

#### There should be an abstract way of compressing this....
# Fill playbook with controller and compute nodes
playTemplate = IO.read('MetaConfig/playbook_template')
File.open(options["playbookPath"], 'w') {|file|
  file.write(playTemplate % {controller_host: controlname, controller_ip: nodes[controlname]['interface']['ip'],compute_hosts: computenames[0..-1].join(", "), controller_address: "http://"+controlname+":5000/v3"})
}
#Fill host.j2 with controller and compute nodes
hostsTemplate = IO.read('MetaConfig/hosts_template')
File.open(options["hostsPath"], 'w'){|file|
  file.write(hostsTemplate)
  nodes.keys.each{|host| file.write(nodes[host]["interface"]["ip"].to_s+" "+host.to_s+"\n")}
}
