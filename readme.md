# Abstract
Set up of an OpenStack Cluster using [Vagrant Virtual Machines][2] (VM) and [Ansible Configuration Playbooks][3]. Controller and Compute machines are created and configured according to the [OpenStack Installation Guide][1].
The whole configuration is adapted to work in a [CentOS 7][4] machine.

Vagrant will set up a certain number (3 by default) of Virtual Machines (VVM), one controller node and 2 compute nodes.
Then, Ansible will provision OpenStack into them, using roles to differentiate between the controller node configuration
and the compute node configuration. At the end of this process one must be able to freely spawn OpenStack instances inside
the compute nodes from the controller node via command line or dashboard (Horizon).

This basic ![topology](Network_Topology.pdf) is explicited in ![Nodes.json](Nodes.json) file. This file tells Vagrant the interfaces
of the VM in wich OpenStack will be provisioned.

**Warning:** OpenStack instances should be configured to use the controller node as a proxy to acces the outer world. Otherwise,
it will not be possible to acces repositories from those instances.

# TL;DR

## Programs and Packages needed
- Vagrant (CentOS/7 box)
- Ansible
- Ruby

## Machine Dictionary
- local: Machine in wich you want to set up VM to set up OpenStack inside of them
- controller: Vagrant VM with _controller_ rol
- compute: Vagrant VM with _compute_ rol

## Set up Ansible files
1. Modify Nodes.json to set up the connectivity specifications of each virtual machines (At least 1 controller and 1 compute)
2. Run _[local]$ ./SetupVagrant.rb -f Nodes.json_ to set up the Ansible files acording to your virtual machine's specifications (requires _Ruby_)

## Launch and acces an instance
1. Up and provision VMs using Ansible playbooks _[local]$ vagrant up --provision_.

2. Acces the dashboard http://controller_ip/dashboard using a browser and log in using suitable credentials.
* Default credentials
  * Domain: Default
  * User: admin
  * Pass: vagrant

3. Press _Project > Compute > Instances > Launch Instance_ to launch an instance using default configuration.

## SSH into an instance (no floating IP)
1. Acces the controller node.
  * _[local]$ vagrant ssh controller_ by default
2. Login as root user.
  * _[controller]$ su root_
  * Password (by default): _vagrant_
3. Use _[controller]$ ip netns_ to obtain the namespace of the router _router\_name_.
4. Use _[controller]$ ip netns exec router\_name ssh user@instance_ip_ to ssh into the instance.
  * Default credentials:
    * user: cirros
    * instance_ip: _Project > Instances_
    * password: gocubsgo

## SSH into an instance (floating IP)
1. Acces the Dashboard (see next subsection)
2. Press _Admin > Network > Floating IPs_ to acces the Floating IP manager
3. Press _Allocate IP To Project_ to create a floating IP.
4. Associate the floating IP to the desired instance.
5. Acces the instance using _[local]$ ssh -i key-to-log-in user@floating-ip_

## Horizon (dashboard)
1. Acces to the Dashboard via http://controller_ip/dashboard
2. Default login credentials (if you followed the [official guide][1]):
  * Domain: default
  * User: admin
  * Password: vagrant

# Technical Issues

## Release
- OpenStack Queens

## Installed Services
- Identity service: Keystone
- Image service: Glance
- Compute service: Nova
- Networking service: Neutron
- Dashboard: Horizon

### Vagrantfile
Sets up a set of virtual machines using configuration parameters provided by GenFiles.rb or default files.

## Nodes.json
Vagrant uses this file to spawn virtual machines. Structure is as follows:

- Hostname:
  * Interface:
    * IP
    * Netmask
    * Name
  * Role

_Hostname_ refers to the host name of the virtual machine spawned by Vagrant (VVM), _Interface_ refers to the interface to be used by the VVMs to communicate between them, and Role explicits the Ansible role to be used. It is related to the Physical network showed in the ![network topology](Network_Topology.pdf)

## Ansible Idempotency

A _check-if-fails_ approach was used to ensure _command_ idempotency inside Ansible playbooks; that is, every group of tasks in which a _command_ play is involved starts with a set of checks which raise errors if they fail, implying that that specific step must be run by Ansible. If there is no faile, Ansible will skip that step since it will be already well provisioned.


## Credential files
There are several credential files in which users, passwords and IP addresses are stored.

### credentials.yml
- Accesible to all instances.
- Contains user/password.
- Contains __controller IP address__ (it has to be accesible to everybody).

### hosts.j2
- Accesible to all instances.
- Lists the existing machines (ip - hostname) in order to allow connection amongst them.

# Set Up: Gotchas & Pitfalls
The whole set up process follows the [OpenStack Installation Guide][1]. Nevertheless, this tutorial is not intended to be performed on a configuration management system such as [Ansible][3]; therefore, we will use this section to point out difficulties which arised during the set up process. __Main task: Ensure Playbook Idempotency__.

## Command Summary
- Use _service_ module to enable and restart service by using _enabled_ and _state_ options.
- Use _register_ to register variables in order to manually check changes (not the best option, but necessary some times).
- Use _when_ to implement _if_ conditionals in single tasks.
- Use _with\_items_ to implement a pythonic _for loop_ on a playbook (repeat the same play with different options)
- _shell_ or _command_ may be used when the action requires an explicit call to an internal OpenStack element.

## Initial Configuration
- Stop and Mask _firewalld_ service since it is enabled by default in CentOS and prevents OpenStack from working properly (OpenStack operates using _iptables_).

## Environtment (Host Networking, NTP ...)
- Download Packages: Use _yum_ module to reach packages.
- Databases: Use _mysql_ module to manage every database during the installation process.
- Configuration Files: Use _template_ module to modify configurations files.
- Ansible __does not__ support to have variables inside other variables: Use different configuration files and a playbook conditional if needed

## MariaDB
- If is the first provision, root user must be given a password inside database.
- Else, no further configuration of root user must be done
This behaiviour is achieved checking for the existence of /root/.my.cnf file.

## Keystone (Identity)Ansible Configuration Playbooks
- We use ignore_errors plays to check for non implemented features and ensure idempotency. This may change in a future.
- Inside this play admin environment variables are loaded using a dictionary inside admin_env.yml. This allows not to use _source_ command in each single play where admin credentials are needed.

## Glance (Image)
- __!__ There are _two_ different glance credentials (glance user in the glance DB and glance user in OpenStack).
- We use ignore_errors plays to check for non implemented features and ensure idempotency. This may change in a future.
- CirrOS image is always downloaded and overwritten. Mounting the image is idempotent by means of ignore_errors.

## Nova (Compute)
- Controller Node:
  - __!__ There are _two_ different nova credentials (nova user in the nova DB and nova user in OpenStack).
  - We use ignore_errors plays to check for non implemented features and ensure idempotency. This may change in a future.
  - When create cell1, return code is 0 if the cell1 is created and 2 if it is already used. We use this to avoid rising and error in such cases by means of changed_when and failed_when.
  - cell0 does not suffer this problem since it returns 0 even when it is already in use.

## Neutron (Network)
- __!__ There are _two_ different neutron credentials (neutron user in the neutron DB and neutron user in OpenStack).
- Follow the [official guide][1].

## Horizon (Dashboard)
- Follow the [official guide][1].
- Acces to the Dashboard via http://controller_ip/dashboard
- Defalt login credentials (if you followed the [official guide][1]):
  * Domain: default
  * User: admin
  * Password: vagrant

[1]: https://docs.openstack.org/install-guide/index.html
[2]: https://www.vagrantup.com/
[3]: https://www.ansible.com/
[4]: https://www.centos.org/
